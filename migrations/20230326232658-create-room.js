'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      creatorId: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'Users',
          key: 'id',
          as: 'creatorId'
        }
      },
      targetId: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'Users',
          key: 'id',
          as: 'targetId'
        }
      },
      result1: {
        type: Sequelize.STRING
      },
      result2: {
        type: Sequelize.STRING
      },
      result3: {
        type: Sequelize.STRING
      },
      creatorChoice1: {
        type: Sequelize.STRING
      },
      creatorChoice2: {
        type: Sequelize.STRING
      },
      creatorChoice3: {
        type: Sequelize.STRING
      },
      targetChoice1: {
        type: Sequelize.STRING
      },
      targetChoice2: {
        type: Sequelize.STRING
      },
      targetChoice3: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Rooms');
  }
};