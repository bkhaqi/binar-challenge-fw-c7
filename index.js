const express = require("express");
require('express-group-routes');
const session = require("express-session");
const cookieParser = require("cookie-parser");
const Sequelize = require("sequelize");


const dotenv = require('dotenv');
dotenv.config();

const SequelizeStore = require("connect-session-sequelize")(session.Store);

const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/config/config.json')[env];

const sequelize = new Sequelize(config.database, config.username, config.password, {
  dialect: config.dialect
});

const router = require("./routes");

const app = express();

app.use(session({
    secret: "secret",
    store: new SequelizeStore({
        db: sequelize
    }),
    saveUninitialized:true,
    resave: false
}));

sequelize.sync();

app.use(cookieParser());

app.set('view engine', 'ejs');

const port = process.env.APP_PORT || "3030";

app.use(router);

app.listen(port, () => {
    console.log(`App listening on port: ${port}`);
})