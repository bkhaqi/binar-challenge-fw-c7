'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Room.belongsTo(models.User, {
        foreignKey: "creatorId"
      });
      Room.belongsTo(models.User, {
        foreignKey: "targetId"
      });
    }
  }
  Room.init({
    creatorId: DataTypes.INTEGER,
    targetId: DataTypes.INTEGER,
    result1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    result2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    result3: {
      type: DataTypes.STRING,
      allowNull: true
    },
    creatorChoice1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    creatorChoice2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    creatorChoice3: {
      type: DataTypes.STRING,
      allowNull: true
    },
    targetChoice1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    targetChoice2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    targetChoice3: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    sequelize,
    modelName: 'Room',
  });
  return Room;
};