'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Biodata.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      });
    }
  }
  Biodata.init({
    userId: {
      type: DataTypes.INTEGER,
      validate: {
        isUnique: (value, next) => {
          Biodata.findAll({
            where: { userId: value }
          })
          .then((biodata) => {
            if (biodata.length != 0) {
              next(new Error('Biodata already exist'));
            }
            next();
          });
        }
      }
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Email: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        isEmail: true
      }
    }
  }, {
    sequelize,
    modelName: 'Biodata',
  });
  return Biodata;
};