const e = require("express");
const { User, Biodata } = require("../../models");

module.exports = {
    login: async (req, res) => {
        let message = {
            type: 'error',
            message: 'Login failed, username/password does not match'
        };

        try {
            let user = await User.findOne({
                where: { username: req.body.username }
            });

            // Password not valid
            if (!user.validPassword(req.body.password)) {
                return res.render("login", {
                    message: message,
                    session : req.session
                })
            }

            // If valid, store to session
            req.session.userId = user.id;
            req.session.username = user.username;
            req.session.isAdmin = user.isAdmin;

            res.redirect("/user");
        } catch (error) {
            return res.render("login", {
                session: req.session,
                message: message
            })
        }
    },
    register: async (req, res) => {
        try {
            let user = await User.create({
                username: req.body.username,
                password: req.body.password
            });

            res.redirect("/login");
        } catch (error) {
            res.render("register", {
                session : req.session,
                message: {
                    type: 'error',
                    message: error
                }
            })
        }
    },
    logout: async (req, res) => {
        if (req.session.userId) {
            req.session.destroy();
            res.redirect("/");
        }
    },
    edit: async (req, res) => {
        try {
            let user = await User.findByPk(req.session.userId);
            user.username = req.body.username ? req.body.username : user.username;
            user.password = req.body.password ? req.body.password : user.password;
            await user.save();

            res.redirect('/user');
        } catch (error) {

            let user = await User.findByPk(req.session.userId);
            res.render('User/edit', {
                error: error,
                session : req.session,
                user: user
            });
            console.log(error)
        }
    },
    editBiodata: async (req, res) => {
        try {
            let biodata = await Biodata.findOne({
                where: { userId: req.session.userId }
            });
            biodata.firstName = req.body.firstName ? req.body.firstName : biodata.firstName;
            biodata.lastName = req.body.lastName ? req.body.lastName : biodata.lastName;
            biodata.Email = req.body.Email ? req.body.Email : biodata.Email;
            await biodata.save();

            res.redirect('/user/biodata');
        } catch (error) {

            let biodata = await Biodata.findOne(req.session.userId);
            res.render('User/Biodata/edit', {
                error: error,
                session : req.session,
                biodata: biodata
            });
            console.log(error)
        }
    },
    createBiodata:  async (req, res) => {
        try {
            let biodata = await Biodata.create({
                id: req.body.id,
                userId: req.body.userId,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                Email: req.body.Email
            });

            res.redirect("/user/biodata");
        } catch (error) {
            res.render("User/Biodata/create", {
                session : req.session,
                message: {
                    type: 'error',
                    message: error
                }
            })
        }
    },
    delete: async (req, res) => {
        try {
            const count = await User.destroy({
                where: { id: req.session.userId }
            });
            

            if (count > 0) {
                req.session.destroy();
                return res.redirect('/');
            } else {
                return res.redirect('/');
            }
        } catch (err) {
            return res.redirect('/');
        }
    },
    createUser: async (req, res) => {
        try {
            let user = await User.create({
                username: req.body.username,
                password: req.body.password
            });

            res.redirect("/user");
        } catch (error) {
            res.render("User/create", {
                session : req.session,
                message: {
                    type: 'error',
                    message: error
                }
            })
        }
    },
    deleteUser: async (req, res) => {
        try {
            const count = await User.destroy({
                where: { id: req.params.id }
            });
            

            if (count > 0) {
                return res.redirect('/user');
            } else {
                return res.redirect('/user');
            }
        } catch (err) {
            return res.redirect('/user');
        }
    }

};