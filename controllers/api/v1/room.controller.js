const { User, Room } = require("../../../models");
const { success, error } = require("../../../utils/response/json.utils");

module.exports = {
    createRoom: async (req, res) => {
        try {
            let target = await User.findOne({
                where: {username: req.body.targetName}
            });
            let room = await Room.create({
                creatorId: req.user.id,
                targetId: target.id
            });

            return success(res, 201, "Room Created", {roomId : room.id});
        } catch (err) {
            return error(res, 400, err.message, {})
        }
    }
}