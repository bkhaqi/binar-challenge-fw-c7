const { User, Biodata , History } = require("../../../models");
const { error, success } = require("../../../utils/response/json.utils");

module.exports = {
    login:  async (req, res) => {
        let message = "Password does not match";
        let adminMsg = "Cannot Play as Admin"
        let errMsg = "error"

        try {
            let user = await User.findOne({
                where: {username: req.body.username}
            });
            
            //Password Validation
            user.validPasswordPromise(req.body.password)
                .then(resolve => {
                    return res.json({
                        message: 'Login successfully',
                        token: user.generateToken()
                    })
                })
                .catch(reject => {
                    return res.status(400).json({
                        message: 'Failed to login'
                    })
                });

        } catch (err) {
            return error(res, 400, errMsg, {});
        }
    },
    register: async (req, res ) => {
        try {
            let user = await User.create({
                username: req.body.username,
                password: req.body.password
            });

            return success(res, 201, 'Register Succes, Please Login');
        } catch (err) {
            return error(res, 400, err.message, {})
        }
    },
    verify: async (req, res) => {
        let user = await User.findOne({
            where: {id: req.user.id}
        });

        return success(res, 200,"OK", {user})
    }
}