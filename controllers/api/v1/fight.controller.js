const { User, Room, History } = require("../../../models");
const { success, error } = require("../../../utils/response/json.utils");

module.exports = {
    fight: async (req, res) => {

        //Get Winner
        const getWinner = function(creator, target) {
            if(creator === 'R' && target === 'S' || creator === 'P' && target === 'R' || creator === 'S' && target === 'P'){
                return 'Creator Win'

            } else if (creator === 'P' && target === 'S' || creator === 'R' && target === 'P' || creator === 'S' && target === 'R' ){
                return 'Target Win'

            } else {
                return 'Draw'
            }
        };

        try {
            let room = await Room.findOne({
                where: { id: req.params.id }
            });

            //Get Result then Insert to History Table
            const resultHistory = async function (r1, r2, r3) {
                if ( 
                    r1 === 'Draw' && r2 === 'Draw' && r3 === 'Draw' || 
                    r1 === 'Creator Win' && r2 === 'Draw' && r3 === 'Target Win' || 
                    r1 === 'Creator Win' && r2 === 'Target Win' && r3 === 'Draw' ||
                    r1 === 'Target Win' && r2 === 'Creator Win' && r3 === 'Draw' || 
                    r1 === 'Target Win' && r2 === 'Draw' && r3 === 'Creator Win' || 
                    r1 === 'Draw' && r2 === 'Target Win' && r3 === 'Creator Win' || 
                    r1 === 'Draw' && r2 === 'Creator Win' && r3 === 'Target Win'
                ) {
                    let result = 'Draw'

                    let creatorHistory = await History.create({
                        userId: room.creatorId,
                        roomId: room.id,
                        result: result
                    });

                    let targetHistory = await History.create({
                        userId: room.targetId,
                        roomId: room.id,
                        result: result
                    });


                } else if (
                    r1 === 'Creator Win' && r2 === 'Creator Win' && r3 === 'Creator Win' || 
                    r1 === 'Creator Win' && r2 === 'Creator Win' && r3 === 'Target Win' ||
                    r1 === 'Creator Win' && r2 === 'Target Win' && r3 === 'Target Win' ||
                    r1 === 'Target Win' && r2 === 'Creator Win' && r3 === 'Creator Win' ||
                    r1 === 'Creator Win' && r2 === 'Creator Win' && r3 === 'Draw' ||
                    r1 === 'Creator Win' && r2 === 'Draw' && r3 === 'Creator Win' ||
                    r1 === 'Draw' && r2 === 'Creator Win' && r3 === 'Creator Win' ||
                    r1 === 'Draw' && r2 === 'Draw' && r3 === 'Creator Win' ||
                    r1 === 'Draw' && r2 === 'Creator Win' && r3 === 'Draw' ||
                    r1 === 'Creator Win' && r2 === 'Draw' && r3 === 'Draw'
                ) {
                    let creator = "Win"
                    let target = "Lose"

                    let creatorHistory = await History.create({
                        userId: room.creatorId,
                        roomId: room.id,
                        result: creator
                    });

                    let targetHistory = await History.create({
                        userId: room.targetId,
                        roomId: room.id,
                        result: target
                    });


                } else {
                    let creator = "Lose"
                    let target = "Win"

                    let creatorHistory = await History.create({
                        userId: room.creatorId,
                        roomId: room.id,
                        result: creator
                    });

                    let targetHistory = await History.create({
                        userId: room.targetId,
                        roomId: room.id,
                        result: target
                    });
                }
            }

            //Check Creator Valid
            if (req.user.id === room.creatorId) {
                if(room.result1 && room.result2 && room.result3){
                    //Check History
                    let historyResult = await History.findOne({
                        where : {
                            userId : req.user.id,
                            roomId: room.id
                        }
                    });

                    //Insert into History 
                    if(!historyResult && room.creatorId == req.user.id){
                        resultHistory(room.result1, room.result2, room.result3);
                    }

                    return success(res, 200, 'Game Finished', {
                        Round_1:{
                            result: room.result1,
                            creator: room.creatorChoice1,
                            target: room.targetChoice1
                        },
                        Round_2:{
                            result: room.result2,
                            creator: room.creatorChoice2,
                            target: room.targetChoice2
                        },
                        Round_3:{
                            result: room.result3,
                            creator: room.creatorChoice3,
                            target: room.targetChoice3
                        }

                    });


                } else if (!room.result1) {
                    if(!room.creatorChoice1) {
                        room.creatorChoice1 = req.body.choice;
                        await room.save();


                        if(!room.targetChoice1) {
                            return success(res, 200, '1st Round: Waiting for Opponent.....', {
                                Round_1: {
                                    creator: room.creatorChoice1,
                                    target: room.targetChoice1
                                }
                            });
                        }

                        let Result1 = getWinner( room.creatorChoice1, room.targetChoice1 );
                        room.result1 = Result1;
                        await room.save();

                        return success(res, 201, `1st Round: ${Result1}`, {
                            Round_1: {
                                creator: room.creatorChoice1,
                                target: room.targetChoice1
                            }
                        });
                    }
                } else if (!room.result2 ) {
                    if(!room.creatorChoice2){
                        room.creatorChoice2 = req.body.choice;
                        await room.save();

                        if (!room.targetChoice2){
                            return success(res, 200, '2nd Round: Waiting for Opponent....', {
                                Round_1: {
                                    creator: room.creatorChoice1,
                                    target: room.targetChoice1
                                },
                                Round_2: {
                                    creator: room.creatorChoice2,
                                    target: room.targetChoice2
                                }
                            });
                        }

                        let Result2 = getWinner(room.creatorChoice2, room.targetChoice2);
                        room.result2 = Result2;
                        await room.save();

                        return success(res, 201, `2nd Round: ${Result2}`, {
                            Round_1: {
                                creator: room.creatorChoice1,
                                target: room.targetChoice1
                            },
                            Round_2: {
                                creator: room.creatorChoice2,
                                target: room.targetChoice2
                            }
                        });
                    }
                } else {
                   if(!room.creatorChoice3 ){
                    room.creatorChoice3 = req.body.choice;
                    await room.save();

                    if(!room.targetChoice3 ){
                        return success(res, 200, '3rd Round: Waiting for Opponent....', {
                            Round_1: {
                                creator: room.creatorChoice1,
                                target: room.targetChoice1
                            },
                            Round_2: {
                                creator: room.creatorChoice2,
                                target: room.targetChoice2
                            },
                            Round_3: {
                                creator: room.creatorChoice3,
                                target: room.targetChoice3
                            }
                        });
                    }

                    let Result3 = getWinner(room.creatorChoice3, room.targetChoice3);
                    room.result3 = Result3;
                    await room.save();

                    return success(res, 200, `3rd Round: ${Result3}`,{
                        Round_1: {
                            creator: room.creatorChoice1,
                            target: room.targetChoice1
                        },
                        Round_2: {
                            creator: room.creatorChoice2,
                            target: room.targetChoice2
                        },
                        Round_3: {
                            creator: room.creatorChoice3,
                            target: room.targetChoice3
                        }
                    });
                   } 
                }
                //Check Target
            } else if (req.user.id === room.targetId) {
                if(room.result1 && room.result2 && room.result3) {
                    //History check
                    let historyResult = await History.findOne({
                        where: {
                            userId: req.user.id,
                            roomId: room.id
                        }
                    });

                    //Insert into History
                    if(!historyResult && room.targetId == req.user.id ) {
                        resultHistory(room.result1, room.result2, room.result3);
                    }
                    return success(res, 200, 'Game Finished', {
                        Round_1:{
                            result: room.result1,
                            creator: room.creatorChoice1,
                            target: room.targetChoice1
                        },
                        Round_2:{
                            result: room.result2,
                            creator: room.creatorChoice2,
                            target: room.targetChoice2
                        },
                        Round_3:{
                            result: room.result3,
                            creator: room.creatorChoice3,
                            target: room.targetChoice3
                        }
                    });
                } else if (!room.result1 ) {
                    if (!room.targetChoice1 ) {
                        room.targetChoice1 = req.body.choice;
                        await room.save();

                        if(!room.creatorChoice1) {
                            return success(res, 200, '1st Round: Waiting for Opponent....', {
                                Round_1: {
                                    creator: room.creatorChoice1,
                                    target: room.targetChoice1
                                }
                            });
                        }

                        let Result1 = getWinner(room.creatorChoice1, room.targetChoice1);
                        room.result1 = Result1
                        await room.save();

                        return success(res, 201, `1st Round: ${Result1}`, {
                            Round_1: {
                                creator: room.creatorChoice1,
                                target: room.targetChoice1
                            }
                        });
                    }
                } else if (!room.result2 ){
                    if (!room.targetChoice2){
                        room.targetChoice2 = req.body.choice;
                        await room.save()

                        if (!room.creatorChoice2) {
                            return success(res, 200, '2nd Round: Waiting for Opponent', {
                                Round_1: {
                                    creator: room.creatorChoice1,
                                    target: room.targetChoice1
                                },
                                Round_2: {
                                    creator: room.creatorChoice2,
                                    target: room.targetChoice2
                                }
                            });
                        }

                        let Result2 = getWinner(room.creatorChoice2, room.targetChoice2);
                        room.result2 = Result2;
                        await room.save();

                        return success(res, 201, `2nd Round: ${Result2}`,{
                            Round_1: {
                                creator: room.creatorChoice1,
                                target: room.targetChoice1
                            },
                            Round_2: {
                                creator: room.creatorChoice2,
                                target: room.targetChoice2
                            }
                        });
                    }
                } else {
                    if (!room.targetChoice3){
                        room.targetChoice3 = req.body.choice;
                        await room.save();

                        if(!room.creatorChoice3) {
                            return success(res, 200, "3rd Round: Waiting for Opponent", {
                                Round_1: {
                                    creator: room.creatorChoice1,
                                    target: room.targetChoice1
                                },
                                Round_2: {
                                    creator: room.creatorChoice2,
                                    target: room.targetChoice2
                                },
                                Round_3: {
                                    creator: room.creatorChoice3,
                                    target: room.targetChoice3
                                }
                            });
                        }

                        let Result3 = getWinner( room.creatorChoice3, room.targetChoice3);
                        room.result3 = Result3;
                        await room.save();

                        return success(res, 201, `3rd Game: ${Result3}`,{
                            Round_1: {
                                creator: room.creatorChoice1,
                                target: room.targetChoice1
                            },
                            Round_2: {
                                creator: room.creatorChoice2,
                                target: room.targetChoice2
                            },
                            Round_3: {
                                creator: room.creatorChoice3,
                                target: room.targetChoice3
                            }
                        });
                    }
                }
            } else {
                return error (res, 401, "Unauthorized not creator or target!",`${room.id}`)
            }
            
        } catch (err) {
            return error(res, 400, err.message, `${req.user.id}`);
        }
        
    }
}