const { User, Biodata, History  } = require("../models");

module.exports = {
    index: async (req, res) => {
        let whereQuery = req.session.isAdmin === true ? {} : { id: req.session.userId };

        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ];
        let includeQuery = [
            {model: Biodata },
            {model: History}
        ];
        User.findAll({
            where: whereQuery,
            order: orderQuery,
            include: includeQuery
        })
        .then(user => {
            res.render('User/index', {
                session: req.session,
                user: user,
                Biodata: Biodata
            });
        });
    },
    biodata: async (req, res) => {
        let whereQuery = req.session.isAdmin === true ? {} : { userId: req.session.userId };

        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ];
        Biodata.findAll({
            where: whereQuery,
            order: orderQuery
        })
        .then(biodata => {
            res.render('User/Biodata/biodata', {
                session: req.session,
                biodata: biodata,
                Bio : Biodata
            });
        });
    },
    edit: async (req, res) => {
        User.findByPk(req.session.userId)
        .then(user => {
            res.render('User/edit', {
                session: req.session,
                user: user
            });
        });
    },
    createBiodata: async (req, res) => {
        Biodata.findAll({
            where: {userId : req.session.userId}
        })
        .then(biodata => {
            res.render('User/Biodata/create', {
                session: req.session,
                biodata: biodata
            });
        });
    },
    editBiodata: async (req, res) => {
        Biodata.findAll({
            where: {userId : req.session.userId}
        })
        .then(biodata => {
            res.render('User/Biodata/edit', {
                session: req.session,
                biodata: biodata
            });
        });
    },
    viewHistory: async (req, res) => {
        let whereQuery = req.session.isAdmin === true ? {} : { userId: req.session.userId };

        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ];
        let includeQuery = [
            {model: User }
        ];

        History.findAll({
            where: whereQuery,
            order: orderQuery,
            include: includeQuery

        })
        .then(history => {
            res.render('User/History/index', {
                session: req.session,
                history: history,
            });
        });
    },
    createUser: async (req, res) => {
        res.render('User/create', {
            session: req.session
        });
    },
    viewBiodata: async (req, res) => {

        let includeQuery = [
            {model: User}
        ];
        Biodata.findAll({
            where: {userId: req.params.id},
            include: includeQuery
        })
        .then(biodata => {
            res.render('User/Biodata/biodata', {
                session: req.session,
                biodata: biodata
            })
        })
    }
}