const userController = require("../controllers/user.controller");
const webMiddleware = require("../middlewares/web.middleware");
const router = require("express").Router();


router.get("/", webMiddleware.verifyWebSession, userController.index);
router.get("/edit",webMiddleware.verifyWebSession,userController.edit);
router.get("/create",webMiddleware.verifyWebSession, userController.createUser);
router.get("/viewBiodata/:id", webMiddleware.verifyWebSession, userController.viewBiodata);

//BIODATA
router.get("/biodata", webMiddleware.verifyWebSession, userController.biodata);
router.get("/biodata/edit", webMiddleware.verifyWebSession, userController.editBiodata);
router.get("/biodata/create", webMiddleware.verifyWebSession, userController.createBiodata);

//HISTORY
router.get("/viewHistory", webMiddleware.verifyWebSession, userController.viewHistory);

module.exports = router;