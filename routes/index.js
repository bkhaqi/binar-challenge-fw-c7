const router = require("express").Router();
const homeRouter = require("./home.router");
const userRouter = require("./user.router");

const apiUserRouter = require("./api/user.router");

// V1 API
const apiUserRouterV1 = require("./api/v1/user.v1.router")
const apiGameRouterV1 = require("./api/v1/game.v1.router")

// MVC
router.use("/", homeRouter);
router.use("/user", userRouter);

// MCR
router.use("/api/user", apiUserRouter);

//V1 API router group
router.group("/api/v1", route => {
    route.use("/user", apiUserRouterV1);
    route.use("/play", apiGameRouterV1);
});
module.exports = router;