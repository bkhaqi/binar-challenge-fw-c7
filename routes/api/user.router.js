const router = require("express").Router();
const bodyParser = require("body-parser");
const userController = require("../../controllers/api/user.controller");

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });


//Only Admin
router.post("/create", urlEncoded, userController.createUser);
router.get("/deleteUser/:id", urlEncoded, userController.deleteUser);


router.post("/login", urlEncoded, userController.login);
router.post("/register", urlEncoded, userController.register);
router.get("/logout", urlEncoded, userController.logout);
router.post("/edit", urlEncoded, userController.edit);
router.get("/delete",urlEncoded,userController.delete);

//Biodata
router.post("/biodata/edit", urlEncoded, userController.editBiodata);
router.post("/biodata/create", urlEncoded, userController.createBiodata);

module.exports = router;