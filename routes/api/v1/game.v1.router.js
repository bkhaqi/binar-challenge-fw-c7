const router = require("express").Router();
const bodyParser = require("body-parser");
require("express-group-routes");

const jsonParser = bodyParser.json();
const roomV1Controller = require("../../../controllers/api/v1/room.controller")
const fightV1Controller = require("../../../controllers/api/v1/fight.controller")
const { verifyJWT } = require("../../../middlewares/api.middleware");


router.post("/create-room", jsonParser, verifyJWT, roomV1Controller.createRoom);
router.post("/fight/:id", jsonParser, verifyJWT, fightV1Controller.fight);

module.exports = router;