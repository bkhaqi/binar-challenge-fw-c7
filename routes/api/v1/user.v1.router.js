const router = require("express").Router();
const bodyParser = require("body-parser");
require("express-group-routes");

const jsonParser = bodyParser.json();
const userV1Controller = require("../../../controllers/api/v1/user.contoller");
const { verifyJWT } = require("../../../middlewares/api.middleware");

router.post("/login", jsonParser, userV1Controller.login);
router.post("/register", jsonParser, userV1Controller.register);
router.get("/verify", jsonParser, verifyJWT, userV1Controller.verify);

module.exports = router;